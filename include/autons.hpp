#pragma once

#include "EZ-Template/drive/drive.hpp"

extern Drive chassis;

void interfered_example();

void default_constants();
void one_mogo_constants();
void two_mogo_constants();
void exit_condition_defaults();
void modified_exit_condition();

void cheese_burgers();
void skills_auton();
void match_auton_left();

void AWP_close();

void cata_shoot(int vel);

void match_auton_left_test();


void far_awp_2();

void anti_rush_far();

void slow_far_auton();

void tristan_auton();

void skills_masterpiece();