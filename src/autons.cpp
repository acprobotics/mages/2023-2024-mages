#include "main.h"
pros::ADIDigitalOut wings(8);
pros::ADIDigitalOut button(2);
extern pros::Motor cata;
extern pros::Motor intake;
extern pros::Rotation tenz;




const int DRIVE_SPEED = 110; // This is 110/127 (around 87% of max speed).  We don't suggest making this 127.
                             // If this is 127 and the robot tries to heading correct, it's only correcting by
                             // making one side slower.  When this is 87%, it's correcting by making one side
                             // faster and one side slower, giving better heading correction.
const int TURN_SPEED  = 90;
const int SWING_SPEED = 90;

//this sucks

//hi
///
// Constants
///

// It's best practice to tune constants when the robot is empty and with heavier game objects, or with lifts up vs down.
// If the objects are light or the cog doesn't change much, then there isn't a concern here.

void default_constants() {
  chassis.set_slew_min_power(80, 80);
  chassis.set_slew_distance(7, 7);
  chassis.set_pid_constants(&chassis.headingPID, 11, 0, 20, 0);
  chassis.set_pid_constants(&chassis.forward_drivePID, 0.45, 0, 5, 0);
  chassis.set_pid_constants(&chassis.backward_drivePID, 0.45, 0, 5, 0);
  chassis.set_pid_constants(&chassis.turnPID, 5, 0.003, 35, 15);
  chassis.set_pid_constants(&chassis.swingPID, 7, 0, 45, 0);
}

void one_mogo_constants() {
  chassis.set_slew_min_power(80, 80);
  chassis.set_slew_distance(7, 7);
  chassis.set_pid_constants(&chassis.headingPID, 11, 0, 20, 0);
  chassis.set_pid_constants(&chassis.forward_drivePID, 0.45, 0, 5, 0);
  chassis.set_pid_constants(&chassis.backward_drivePID, 0.45, 0, 5, 0);
  chassis.set_pid_constants(&chassis.turnPID, 5, 0.003, 35, 15);
  chassis.set_pid_constants(&chassis.swingPID, 7, 0, 45, 0);
}

void two_mogo_constants() {
  chassis.set_slew_min_power(80, 80);
  chassis.set_slew_distance(7, 7);
  chassis.set_pid_constants(&chassis.headingPID, 11, 0, 20, 0);
  chassis.set_pid_constants(&chassis.forward_drivePID, 0.45, 0, 5, 0);
  chassis.set_pid_constants(&chassis.backward_drivePID, 0.45, 0, 5, 0);
  chassis.set_pid_constants(&chassis.turnPID, 5, 0.003, 35, 15);
  chassis.set_pid_constants(&chassis.swingPID, 7, 0, 45, 0);
}

void exit_condition_defaults() {
  chassis.set_exit_condition(chassis.turn_exit, 100, 3, 500, 7, 500, 500);
  chassis.set_exit_condition(chassis.swing_exit, 100, 3, 500, 7, 500, 500);
  chassis.set_exit_condition(chassis.drive_exit, 80, 50, 300, 150, 500, 500);
}

void modified_exit_condition() {
  chassis.set_exit_condition(chassis.turn_exit, 100, 3, 500, 7, 500, 500);
  chassis.set_exit_condition(chassis.swing_exit, 100, 3, 500, 7, 500, 500);
  chassis.set_exit_condition(chassis.drive_exit, 80, 50, 300, 150, 500, 500);
}

///
// Interference example
///
void tug (int attempts) {
  for (int i=0; i<attempts-1; i++) {
    // Attempt to drive backwards
    printf("i - %i", i);
    chassis.set_drive_pid(-12, 127);
    chassis.wait_drive();

    // If failsafed...
    if (chassis.interfered) {
      chassis.reset_drive_sensor();
      chassis.set_drive_pid(-2, 20);
      pros::delay(1000);
    }
    // If robot successfully drove back, return
    else {
      return;
    }
  }
}

// If there is no interference, robot will drive forward and turn 90 degrees. 
// If interfered, robot will drive forward and then attempt to drive backwards. 
void interfered_example() {
 chassis.set_drive_pid(24, DRIVE_SPEED, true);
 chassis.wait_drive();

 if (chassis.interfered) {
   tug(3);
   return;
 }

 chassis.set_turn_pid(90, TURN_SPEED);
 chassis.wait_drive();
}


//testing

// . . .
// Make your own autonomous functions here!
// . . .


   void cheese_burgers() {
    //do nothing
   }

   void skills_auton() {
    chassis.set_drive_pid(7.75, DRIVE_SPEED, 0);
    chassis.wait_drive();
    chassis.set_turn_pid(71.5, TURN_SPEED);
    chassis.wait_drive();
    chassis.set_drive_pid(-11, DRIVE_SPEED, 0);
    chassis.wait_drive();
    wings.set_value(1);
    cata.move_velocity(55);
    pros::delay(30000);
    cata.move_velocity(0);
    wings.set_value(0); 
    pros::delay(100);
    chassis.set_turn_pid(135, TURN_SPEED);
    chassis.wait_drive();
    chassis.set_drive_pid(8.5, DRIVE_SPEED, 0);
    chassis.wait_drive();
    chassis.set_turn_pid(90, TURN_SPEED);
    chassis.wait_drive();
    chassis.set_drive_pid(58.5, DRIVE_SPEED, 1);
    chassis.wait_drive();
    chassis.set_turn_pid(0, TURN_SPEED);
    chassis.wait_drive();
    chassis.set_drive_pid(16, DRIVE_SPEED, 0);
    chassis.wait_drive();
    chassis.set_turn_pid(-90, TURN_SPEED);
    chassis.wait_drive();
    chassis.set_drive_pid(15, DRIVE_SPEED, 1);
    chassis.wait_drive();
    chassis.set_turn_pid(-135, TURN_SPEED);
    chassis.wait_drive();
    chassis.set_drive_pid(-33, DRIVE_SPEED, 1);
    chassis.wait_drive();
    chassis.set_drive_pid(12, DRIVE_SPEED, 0);
    chassis.wait_drive();
    chassis.set_turn_pid(-20, TURN_SPEED);
    chassis.wait_drive();
    chassis.set_drive_pid(30, DRIVE_SPEED, 1);
    chassis.wait_drive();
    chassis.set_turn_pid(-77, TURN_SPEED);
    wings.set_value(1);
    chassis.wait_drive();
    chassis.set_drive_pid(-34, DRIVE_SPEED, 0);
    // chassis.set_turn_pid(-120, TURN_SPEED);
    // chassis.set_drive_pid(15, DRIVE_SPEED, true);
    // chassis.wait_drive();
    // chassis.set_angle(0);
    // chassis.set_turn_pid(15, TURN_SPEED);
    // chassis.set_drive_pid(50, DRIVE_SPEED, true);
    // chassis.wait_drive();
    // chassis.set_angle(0);
    // wings.set_value(1);
    // chassis.set_turn_pid(45, TURN_SPEED);
    // chassis.set_drive_pid(12, DRIVE_SPEED, 0);
    // chassis.wait_drive();
    // chassis.set_drive_pid(-12, DRIVE_SPEED, 0);
    // chassis.wait_drive();
    // chassis.set_turn_pid(90, TURN_SPEED);
    // chassis.set_drive_pid(36, DRIVE_SPEED, true);
    // chassis.set_turn_pid(0, TURN_SPEED);
    // chassis.wait_drive();
    // chassis.set_drive_pid(16, DRIVE_SPEED, 1);
   }

   void match_auton_left() {
    chassis.set_drive_pid(-23, 100, false);
    chassis.wait_drive();
    chassis.set_drive_pid(20, 80, false);
    chassis.wait_drive();
    wings.set_value(1);
    chassis.set_turn_pid(-41, 70);

    pros::delay(1500);
    intake.move_velocity(-600);
    chassis.set_drive_pid(30, 50, false);
    chassis.wait_drive();

  
   }

   void match_auton_right() {
    
   }

   void match_auton_left_opps() {
    chassis.set_drive_pid(40, 127, false);
    wings.set_value(1);
    chassis.wait_drive();
    chassis.set_turn_pid(-45, TURN_SPEED);
    chassis.wait_drive();
    chassis.set_drive_pid(8, 127, false);
    chassis.wait_drive();
    chassis.set_turn_pid(45, 70);
    chassis.wait_drive();
    chassis.set_drive_pid(10, 127, false);
    
    // chassis.set_drive_pid(-25, 80);
    // chassis.wait_drive();
    // chassis.set_drive_pid(10, 80);
    // chassis.wait_drive();
    // chassis.set_turn_pid(-45, TURN_SPEED);
    // chassis.set_drive_pid(28, DRIVE_SPEED, true);
   }

   void match_auton_left_test() {
    chassis.set_angle(0);
    chassis.set_drive_pid(-38, DRIVE_SPEED, false);
    chassis.wait_drive();
    chassis.set_turn_pid(-45, TURN_SPEED);
    chassis.set_drive_pid(8, DRIVE_SPEED, false);
    chassis.wait_drive();
    chassis.set_drive_pid(24, DRIVE_SPEED, false);
    chassis.wait_drive();
    chassis.set_turn_pid(-45,TURN_SPEED);
    wings.set_value(1);
    chassis.set_turn_pid(45, TURN_SPEED);
    chassis.set_drive_pid(30, DRIVE_SPEED, false);

    // chassis.set_drive_pid(22, DRIVE_SPEED, false); // Get team triball into team goal
    // chassis.wait_drive();
    // chassis.set_drive_pid(-22, DRIVE_SPEED, false); // Go back to start position
    // chassis.wait_drive();
    // chassis.set_turn_pid(-45, TURN_SPEED);  // Turn 45 to get in position
    // chassis.wait_drive();
    // chassis.set_drive_pid(-10, DRIVE_SPEED, false); // Drive forward to line up with triball
    // chassis.wait_drive();
    // chassis.set_turn_pid(-45, TURN_SPEED); // Rotate to get triball out of match load zone and lnie up with elevation bar
    // chassis.wait_drive();
    // chassis.set_drive_pid(-20, DRIVE_SPEED, false); // Drive towards elevation bar
    // chassis.wait_drive();
// 

   }

   void AWP_close() {

chassis.set_drive_pid(-18.5, 50, true);
chassis.wait_drive();

chassis.set_turn_pid(30, 80);


chassis.set_drive_pid(8, 90, false);
chassis.wait_drive();

chassis.set_drive_pid(-20, 120, true);
chassis.wait_drive();

chassis.set_drive_pid(12, 90, true);
chassis.wait_drive();

wings.set_value(true);


chassis.set_turn_pid(-26, 100);
chassis.wait_drive();

chassis.set_drive_pid(9, 100, true);
chassis.wait_drive();



chassis.set_drive_pid(5, 70, true);
chassis.wait_drive();

chassis.set_turn_pid(-60, 100);
chassis.wait_drive();

wings.set_value(false);

intake.move_velocity(-600);

chassis.set_drive_pid(17.5, 50, true);
chassis.wait_drive();
}

// void team_red_side_left() {
//   //drives backwards with the triball behind it
//   cata.move_relative(---);
//   chassis.set_drive_pid(-15, DRIVE_SPEED, true);

//   //corner triball pick and launch
//   chassis.set_turn_pid(45, TURN_SPEED);
//   chassis.set_drive_pid(10, DRIVE_SPEED, true);
//   intake.move_voltage(10000);
//   chassis.set_turn_pid(90, TURN_SPEED);
//   cata.move_relative(---);

//   //move to the thing and wings kick it out
//   chassis.set_drive_pid(30, DRIVE_SPEED, true);
//   chassis.set_turn_pid(135, TURN_SPEED);
//   wings.set_value(1);

//   //move to the bar to touch it
//   chassis.set_turn_pid(90, TURN_SPEED);
//   chassis.set_drive_pid(15, DRIVE_SPEED, true);
//   chassis.set_turn_pid(95, TURN_SPEED);
// }


void far_awp_2() {
  //team triball to middle
  chassis.set_angle(0);
  chassis.set_drive_pid(40, 80, false);
  chassis.wait_drive();

  //team triball in goal via intake/outtake
  chassis.set_turn_pid(90, TURN_SPEED);
  chassis.wait_drive();

  chassis.set_drive_pid(10, 50, false);
  chassis.wait_drive();

  // intake.move_velocity(-600);
  // pros::delay(100);
  // intake.move_velocity(0);

  //approach close mid triball 
  chassis.set_drive_pid(-8, 50, false);
  chassis.wait_drive();

  chassis.set_turn_pid(240, TURN_SPEED);
  chassis.wait_drive();
  
  //intake close mid triball and approach goal
  // intake.move_velocity(600);
  
  chassis.set_drive_pid(20, 80, false);
  chassis.wait_drive();

  chassis.set_drive_pid(-20, 80, false);
  chassis.wait_drive();
  
  chassis.set_turn_pid(90, TURN_SPEED);

  // //outake triball into goal
  // chassis.set_drive_pid(7.5, 80, false);
  // intake.move_velocity(-600);
  // pros::delay(100);
  // intake.move_velocity(0);

  // //approach middle triballs and intake center one
  // chassis.set_drive_pid(-35, 80, false);
  // chassis.wait_drive();

  // chassis.set_turn_pid(-90, TURN_SPEED);
  // chassis.wait_drive();

  // intake.move_velocity(600);
  
  // chassis.set_drive_pid(8, 80, false);
  // chassis.wait_drive();
}

void anti_rush_far() {
  wings.set_value(1);
  intake.move_velocity(600);

  chassis.set_drive_pid(10, DRIVE_SPEED, false);
  chassis.wait_drive();
  pros:: delay (700);
  wings.set_value(0);

  chassis.set_drive_pid(42, DRIVE_SPEED,false);
  chassis.wait_drive();

  chassis.set_turn_pid(130, TURN_SPEED);
  intake.move_velocity(0);

  pros:: delay (1000);
  wings.set_value(1);

  chassis.set_drive_pid(25, DRIVE_SPEED, false);
  chassis.wait_drive();
  pros:: delay (250);
  wings.set_value(0);

  chassis.set_drive_pid(-30, DRIVE_SPEED, false);
}

void slow_far_auton() {
  //put team triball on line next to the end of the tunnel bar and then put robot's back against the team triball, facing elevation bar triball
  intake.move_velocity(600);
  chassis.set_drive_pid(5, DRIVE_SPEED, false);
  chassis.wait_drive();

  chassis.set_drive_pid(-21, 30, false);
  chassis.wait_drive();
  wings.set_value(1);
  // chassis.set_turn_pid(45, TURN_SPEED);

  chassis.set_turn_pid(123, TURN_SPEED);

  // wings.set_value(0);
  chassis.set_drive_pid(29, DRIVE_SPEED,false);
  chassis.wait_drive();

  // wings.set_value(true);
  // chassis.set_drive_pid(-10, 50, false);

  
}

void tristan_auton() {
  chassis.set_drive_pid(-25, 120, true, false);
  chassis.wait_drive();
  chassis.set_drive_pid(23, DRIVE_SPEED, false);
  chassis.wait_drive();
  chassis.set_drive_pid(-25, 120, true, false);
  chassis.wait_drive();
  chassis.set_drive_pid(23, DRIVE_SPEED, false);
  chassis.wait_drive();

}

void skills_masterpiece() {
  //Start getting triballs into front of goal
  master.print(2, 0, "6030M message: %i", "ERROR");

  //get the red triballs in the goal
  chassis.set_drive_pid(-18.5, 50, 1);
  chassis.wait_drive();
  chassis.set_turn_pid(30, 80);
  chassis.set_angle(0);
  chassis.set_drive_pid(8.5, 90, 0);
  pros::delay(500);
  chassis.set_drive_pid(-10, 120, 0);
  chassis.wait_drive();

  //head towards the match load area
  chassis.set_drive_pid(12, 90, 0);
  chassis.wait_drive();
  
  chassis.set_turn_pid(-45, TURN_SPEED);
  chassis.set_drive_pid(13, DRIVE_SPEED, 0);

  //turn and shoot
  chassis.set_turn_pid(-71.5, TURN_SPEED);
  chassis.wait_drive();
  chassis.set_drive_pid(-11, DRIVE_SPEED, 0);
  chassis.wait_drive();

  //shoot
  wings.set_value(1);
  cata.move_velocity(55);
  pros::delay(30000);
  cata.move_velocity(0);
  wings.set_value(0); 
  pros::delay(100);

  //drive to point A
  chassis.set_turn_pid(-43, TURN_SPEED); // value will be wrong here
  chassis.wait_drive();
  chassis.set_drive_pid(-8.5, DRIVE_SPEED, 0);
  chassis.wait_drive();
  chassis.set_turn_pid(90, TURN_SPEED);
  chassis.wait_drive();
  chassis.set_drive_pid(41.5, DRIVE_SPEED, 1);
  chassis.wait_drive();

  chassis.set_angle(0); //start A end z
  chassis.set_turn_pid(-48, TURN_SPEED);
  chassis.set_drive_pid(-28.60, DRIVE_SPEED, 0);
  chassis.wait_drive();

  chassis.set_angle(0); //start z go B end z
  chassis.set_turn_pid(48, TURN_SPEED); //angle might not be correct
  chassis.set_drive_pid(-10.019, DRIVE_SPEED, 0);
  chassis.wait_drive();
  chassis.set_drive_pid(10.019, DRIVE_SPEED, 0);
  chassis.wait_drive();

  // jocelyn was here 
  chassis.set_angle(0); //start z end C
  chassis.set_turn_pid(-87.2, TURN_SPEED);
  chassis.set_drive_pid(-51.46, DRIVE_SPEED, 1);
  chassis.wait_drive();

  chassis.set_angle(0); //start C end D
  chassis.set_turn_pid(91.3, TURN_SPEED);
  chassis.set_drive_pid(-15.7, DRIVE_SPEED, 1);
  chassis.wait_drive();

  chassis.set_angle(0); //start D to E back to D
  chassis.set_turn_pid(72.7, TURN_SPEED);
  chassis.set_drive_pid(-38.78, DRIVE_SPEED, 1);
  chassis.wait_drive();
  chassis.set_drive_pid(38.78, DRIVE_SPEED, 1);
  chassis.wait_drive();

  chassis.set_turn_pid(0, TURN_SPEED); //start D end F
  chassis.set_drive_pid(-17.8, DRIVE_SPEED, 1);
  chassis.wait_drive();

  chassis.set_angle(0); //start F go G end F
  chassis.set_turn_pid(83.3, TURN_SPEED);
  chassis.set_drive_pid(-37.76, DRIVE_SPEED, 1);
  chassis.wait_drive();
  chassis.set_drive_pid(37.76, DRIVE_SPEED, 1);
  chassis.wait_drive();

  chassis.set_turn_pid(0, TURN_SPEED); //start F end H
  chassis.set_drive_pid(-38.5, DRIVE_SPEED, 1);
  chassis.wait_drive();

  chassis.set_angle(0); //start H end I
  chassis.set_turn_pid(122.2, TURN_SPEED);
  chassis.set_drive_pid(-43.73, DRIVE_SPEED, 1);
  chassis.wait_drive();
}

//cata.more_velocity(55);