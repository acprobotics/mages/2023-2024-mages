#include "main.h"
#include "autons.hpp"

pros::Motor cata(20, MOTOR_GEAR_RED, false, pros::E_MOTOR_ENCODER_DEGREES);
pros::Motor intake(14, MOTOR_GEAR_BLUE, false, pros::E_MOTOR_ENCODER_DEGREES);

// wraps bruh

bool wings_out = false;
bool cata_moving = false;
int cata_speed = 50;
int counter = 0;
int hang_count = 0;
int lift_bool = false;

void cata_shoot(int new_speed)
{
  cata.move_velocity(new_speed);
  cata_moving = true;
}
// among us
// tsst

// Chassis constructor
Drive chassis(
    // Left Chassis Ports (negative port will reverse it!)
    {-19, -16, -15} //-19, -16, -15

    // Right Chassis Ports (negative port will reverse it!)
    ,
    {3, 4, 5} // 3, 4, 5

    // IMU Port
    ,
    18 // 18

    // Wheel Diameter (Remember, 4" wheels are actually 4.125!)
    //    (or tracking wheel diameter)
    ,
    2.75

    // Cartridge RPM
    //   (or tick per rotation if using tracking wheels)
    ,
    200

    // External Gear Ratio (MUST BE DECIMAL)
    //    (or gear ratio of tracking wheel)
    // eg. if your drive is 84:36 where the 36t is powered, your RATIO would be 2.333.
    // eg. if your drive is 36:60 where the 60t is powered, your RATIO would be 0.6.
    ,
    0.75

);

// Ayaan is the best notebooker in the world

void initialize()
{

  pros::delay(300); // Stop the user from doing anything while legacy ports configure.

  // Configure your chassis controls
  chassis.toggle_modify_curve_with_controller(false);
  chassis.set_active_brake(0.01);  // 0.01
  chassis.set_curve_default(1, 0); // Defaults for curve. If using tank, only the first parameter is used. (Comment this line out if you have an SD card!)
  default_constants();             // Set the drive to your own constants from autons.cpp!
  exit_condition_defaults();       // Set the exit conditions to your own constants from autons.cpp!
  cata.set_zero_position(0);

  // These are already defaulted to these buttons, but you can change the left/right curve buttons here!
  // chassis.set_left_curve_buttons (pros::E_CONTROLLER_DIGITAL_LEFT, pros::E_CONTROLLER_DIGITAL_RIGHT); // If using tank, only the left side is used.
  // chassis.set_right_curve_buttons(pros::E_CONTROLLER_DIGITAL_Y,    pros::E_CONTROLLER_DIGITAL_A);

  // Autonomous Selector using LLEMU
  ez::as::auton_selector.add_autons({

      Auton("Skills Master testing", skills_masterpiece),

      Auton("Skills auton", skills_auton),

      Auton("AWP for close side", AWP_close),

      Auton("Front Back 20", tristan_auton),

      Auton("slow awp close", slow_far_auton),

      Auton("Use this for far side anti rush strat", anti_rush_far),

      Auton("match auton left test", match_auton_left),

      Auton("match auton left", match_auton_left_test),

      Auton("Do nothing", cheese_burgers),
  });

  // Initialize chassis and auton selector
  chassis.initialize();
  ez::as::initialize();
}

/**
 * Runs while the robot is in the disabled state of Field Management System or
 * the VEX Competition Switch, following either autonomous or opcontrol. When
 * the robot is enabled, this task will exit.
 */
void disabled()
{
}

/**
 * Runs after initialize(), and before autonomous when connected to the Field
 * Management System or the VEX Competition Switch. This is intended for
 * competition-specific initialization routines, such as an autonomous selector
 * on the LCD.
 *
 * This task will exit when the robot is enabled and autonomous or opcontrol
 * starts.
 */
void competition_initialize()
{
  //. . .
}
//kill the macro

/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */
void autonomous()
{
  chassis.reset_pid_targets();               // Resets PID targets to 0
  chassis.reset_gyro();                      // Reset gyro position to 0
  chassis.reset_drive_sensor();              // Reset drive sensors to 0
  chassis.set_drive_brake(MOTOR_BRAKE_HOLD); // Set motors to hold.  This helps autonomous consistency.

  ez::as::auton_selector.call_selected_auton(); // Calls selected auton from autonomous selector.
}

void opcontrol()
{
  // This is preference to what you like to drive on.

  chassis.set_drive_brake(MOTOR_BRAKE_COAST);
  pros::ADIDigitalOut wings(8);
  pros::ADIDigitalOut lift(2);
  while (true)
  {

    chassis.tank(); // Tank control
    // chassis.arcade_standard(ez::SPLIT); // Standard split arcade
    // chassis.arcade_standard(ez::SINGLE); // Standard single arcade
    // chassis.arcade_flipped(ez::SPLIT); // Flipped split arcade
    // chassis.arcade_flipped(ez::SINGLE); // Flipped single arcade

    // . . .
    // Put more user control code here!
    // . . .
    // if (master.get_digital_new_press(pros::E_CONTROLLER_DIGITAL_L2)){
    //   if (!wings_out){
    //     wings_out = true;
    //     wings.set_value(1);
    //     pros::delay(500);
    //     wings.set_value(0);
    //     wings_out = false;
    //   }
    // }
    // among us testing
    if (master.get_digital_new_press(pros::E_CONTROLLER_DIGITAL_R1))
    {
      if (!wings_out)
      {
        wings.set_value(1);
        wings_out = true;
      }
      else if (wings_out)
      {
        wings.set_value(0);
        wings_out = false;
      }
    }

    if (master.get_digital(pros::E_CONTROLLER_DIGITAL_L1))
    {
      intake.move_velocity(600);
    }
    else if (master.get_digital(pros::E_CONTROLLER_DIGITAL_L2))
    {
      intake.move_velocity(-600);
    }
    else
    {
      intake.move_velocity(0);
    }

    if (master.get_digital_new_press(pros::E_CONTROLLER_DIGITAL_R2))
    {
      cata_moving = !cata_moving;
    }

    if (cata_moving)
    {
      cata.move_velocity(cata_speed);
    }
    else
    {
      cata.move_velocity(0);
    }

    if (master.get_digital_new_press(pros::E_CONTROLLER_DIGITAL_A))
    {
      if (!lift_bool)
      {
        lift.set_value(1);
        lift_bool = true;
      }
      else if (lift_bool)
      {
        lift.set_value(0);
        lift_bool = false;
      }
    }

    if (master.get_digital_new_press(pros::E_CONTROLLER_DIGITAL_DOWN) && cata_speed > 30)
    {
      cata_speed -= 5;
      if (cata_moving)
      {
        cata_shoot(cata_speed);
      }
    }
    if (master.get_digital_new_press(pros::E_CONTROLLER_DIGITAL_UP) && cata_speed < 100)
    {
      cata_speed += 5;
      if (cata_moving)
      {
        cata_shoot(cata_speed);
      }
    }
    if (counter % 20)
    {
      master.print(2, 0, "SPEED: %i        ", (int)cata_speed);
    }
    counter++;
    pros::delay(ez::util::DELAY_TIME); // This is used for timer calculations!  Keep this ez::util::DELAY_TIME
  }
}